#include "gtest/gtest.h"
#include <ezhic/Carrier.h>
#include <ezhic/PhoneRecordPredicate.h>
#include <ezhic/Record.h>
#include <memory>

using namespace ezhic;

namespace {

static char funcptr()
{
    return 17;
}

class AlwaysBreak : public Circuitry::PhoneRecordPredicateCloneable<AlwaysBreak>
{
    virtual bool operator()(const PhoneRecords &history) const { return true; }
};

class NeverBreak : public Circuitry::PhoneRecordPredicateCloneable<NeverBreak>
{
    virtual bool operator()(const PhoneRecords &history) const { return false; }
};

class BreakIfLenGT1 : public Circuitry::PhoneRecordPredicateCloneable<BreakIfLenGT1>
{
    virtual bool operator()(const PhoneRecords &history) const
    {
        return history.size() > 1;
    }
};
}  // namespace

TEST(IntentionTagger, AlwaysBreakNeverRuns)
{
    CallingStrategy intentionTagger;
    intentionTagger.cloneIn(CallingStrategy::SHOULD_BREAK, AlwaysBreak());

    Contact lbl(test_info_->name());
    Bundle bundle;
    bundle.replaceWithClone(intentionTagger);

    EXPECT_EQ(Record::STRAIGHT_TO_VOICEMAIL, bundle.nextRunIntention(lbl));
}

TEST(IntentionTagger, NeverBreakAlwaysRuns)
{
    Contact lbl(test_info_->name());
    Bundle bundle;
    CallingStrategy intentionTagger;
    intentionTagger.cloneIn(CallingStrategy::SHOULD_BREAK, NeverBreak());
    bundle.replaceWithClone(intentionTagger);

    EXPECT_EQ(Record::REGULAR_CALL, bundle.nextRunIntention(lbl));
}

TEST(IntentionTagger, contact_event_trace_passed_to_intention_tagger)
{
    Contact lbl(test_info_->name());
    Circuit &lblCircuit = ezreg::circuit(lbl);
    lblCircuit.acceptUpdate(PhoneCall());

    Bundle bundle;
    CallingStrategy intentionTagger;
    intentionTagger.cloneIn(CallingStrategy::SHOULD_BREAK, BreakIfLenGT1());
    bundle.replaceWithClone(intentionTagger);

    EXPECT_EQ(Record::REGULAR_CALL, bundle.nextRunIntention(lbl));
    lblCircuit.acceptUpdate(PhoneCall());
    EXPECT_EQ(Record::STRAIGHT_TO_VOICEMAIL, bundle.nextRunIntention(lbl));
    Contact newContact(std::string(test_info_->name()) + "otherContact");
    EXPECT_EQ(Record::REGULAR_CALL, bundle.nextRunIntention(newContact));
}

TEST(TestMBundle, MetaBundlePopulates)
{
    Contact lbl(test_info_->name());
    std::stringstream sout;
    Bundle bundle = make_mbundle(
        sout, 100u, std::make_pair(2u, 4u), std::make_pair(2u, 4u), 2u, 2u);
    ezreg::writeBundle(lbl, bundle);
    Carrier(lbl).call(&funcptr);
    // TNullable<char> result =
    // EXPECT_EQ(funcptr(), result.value());
}
