
#include "gtest/gtest.h"

#include "ezhic/PhoneCall.h"
#include "Helpers.h"

#include <bsl_stdexcept.h>

using namespace ezhic;

TEST(TestPhoneCall, DefaultConstructedHas_default_event_ie_Nil)
{
    PhoneCall phoneCall;
    EXPECT_EQ(Record(), phoneCall.record());
    EXPECT_EQ(Record::NIL, phoneCall.record().type());
}

TEST(TestPhoneCall, DefaultConstructed_has_no_duration)
{
    PhoneCall phoneCall;
    EXPECT_FALSE(phoneCall.record().hasKnownDuration());
}

class ThrowingPhoneCall : public PhoneCall
{
  public:
    virtual void call() { throw bsl::runtime_error("test"); }
};

TEST(TestPhoneCall, LongPhoneCallsHaveNonZeroDuration)
{
    PhoneCall phoneCall;
    MonotonicDummy timer;
    phoneCall.timer(&timer);
    phoneCall();
    EXPECT_GT(phoneCall.record().duration(), 0);
}

TEST(TestPhoneCall, ThrowingPhoneCallIsHandled)
{
    ThrowingPhoneCall phoneCall;
    phoneCall();
    EXPECT_EQ(Record::EXCEPTION, phoneCall.record().type());
}
