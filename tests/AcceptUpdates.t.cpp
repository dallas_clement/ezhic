#include "gtest/gtest.h"
#include <ezhic/Carrier.h>
#include <ezhic/StreamLogger.h>

#include <bsl_vector.h>
#include <bsl_sstream.h>

using namespace ezhic;

namespace {
static void dummy()
{
}
}

struct AcceptArrayTest : public ::testing::Test
{
    AcceptArrayTest()
    : aca(), out(), streamLogger(out), phoneCall(), phoneCallDuration(1)
    {
        phoneCall.record().duration(phoneCallDuration);
    }
    AcceptorsArray aca;
    bsl::stringstream out;
    StreamLogger streamLogger;
    PhoneCall phoneCall;
    Record::duration_t phoneCallDuration;
};

TEST_F(AcceptArrayTest, AcceptorsArray_with_logger_and_timeout_logs_and_tags)
{
    // The stream logger should stream out the record from the phoneCall.
    bsl::stringstream expectedStream;
    expectedStream << phoneCall.record() << '\n';
    // First, Add a logger acceptor
    aca.cloneIn(streamLogger);
    // Then add a timeout tagger with shorter lower bound than the phoneCall
    // duration
    aca.cloneIn(TimeoutTagger(phoneCallDuration - 1));

    Record outcome = aca.acceptUpdate(&phoneCall);

    EXPECT_EQ(out.str(), expectedStream.str());
    EXPECT_EQ(outcome.type(), Record::TIMEOUT);
}

TEST_F(AcceptArrayTest,
       Reversed_AcceptorsArray_with_logger_and_timeout_logs_but_doesnt_tag)
{
    // First, add a timeout tagger with shorter lower bound than the phoneCall
    // duration
    aca.cloneIn(TimeoutTagger(phoneCallDuration - 1));
    // Second, add a logger acceptor (any tagging from the timeout will be
    // overwritten by this acceptor)
    bsl::stringstream expectedStream;
    expectedStream << phoneCall.record() << '\n';
    aca.cloneIn(streamLogger);

    Record outcome = aca.acceptUpdate(&phoneCall);

    EXPECT_EQ(out.str(), expectedStream.str());
    EXPECT_NE(outcome.type(), Record::TIMEOUT);
}

TEST(TestAcceptorsInBundle, Logger_acceptor_logs_records_through_bundle)
{
    const Contact contact(test_info_->name());
    Bundle bundle = ezreg::getBundle(contact);
    bsl::stringstream out;
    StreamLogger streamLogger(out);
    bundle.replaceWithClone(streamLogger);
    ezreg::writeBundle(contact, bundle);
    Carrier caller(contact);
    caller.call(dummy);
    EXPECT_NE(out.str(), "");
}

TEST(makeABslVector, checkIcanDoThis)
{
    bsl::vector<int> ints;
    ints.push_back(3);
    EXPECT_TRUE(true);
}