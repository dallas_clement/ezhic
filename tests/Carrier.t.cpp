#include "Helpers.h"
#include "gtest/gtest.h"

#include <ezhic/Carrier.h>

#include <bsl_memory.h>
#include <bsl_stdexcept.h>

#include <ezhic/Bundle.h>
#include <ezhic/PhoneRecordPredicate.h>
#include <ezhic/CallingStrategy.h>

using namespace ezhic;

namespace {

static char g_result;

char NullaryFChar()
{
    return g_result = 17;
}
void NullaryFVoid()
{
    return;
}

char UnaryFChar(int)
{
    return g_result = 19;
}
void UnaryFVoid(int)
{
    return;
}

char BinaryFChar(int, float)
{
    return g_result = 21;
}

int Throw()
{
    throw bsl::invalid_argument("...");
    return 0;
}

struct BNullaryTestChar
{
  public:
    typedef char result_type;

    result_type operator()() const { return g_result = 23; }
    result_type constmember() const { return g_result = 25; }
    result_type member() { return g_result = 27; }
};

struct BNullaryTestVoid
{
  public:
    typedef void result_type;
    void operator()() const { g_result = 28; }
    result_type constmember() const { g_result = 30; }
    result_type member() { g_result = 32; }
};

struct BUnaryTestChar
{
    typedef char result_type;
    typedef int argument_type;

    result_type operator()(int) const { return g_result = 29; }
    result_type constmember(int) const { return g_result = 31; }
    result_type constrefmember(int &v) const { return g_result = v = 33; }
    result_type member(int) { return g_result = 35; }
};

struct BBinaryTestChar
{
    typedef char result_type;
    typedef int first_argument_type;
    typedef float second_argument_type;

    result_type operator()(int, float) const { return g_result = 37; }
    result_type constmember(int, float) const { return g_result = 39; }
    result_type constrefmember(int &v, float &f) const
    {
        return g_result = v = f = 41;
    }
    result_type member(int, float) { return g_result = 43; }
};

struct BUnaryTestVoid
{
    typedef void result_type;
    typedef int argument_type;
    result_type operator()(int) const { g_result = 34; }
    result_type constmember(int) const { g_result = 36; }
    result_type member(int) { g_result = 38; }
};
}  // namespace

class EzhicBundleTest : public ::testing::Test
{
  protected:
    EzhicBundleTest() : timer(), reg(&Registry::instance()), contact(), brp()
    {
        reg->cloneInTimer(timer);
        const ::testing::TestInfo *const test_info =
            ::testing::UnitTest::GetInstance()->current_test_info();
        contact = bsl::string(test_info->test_case_name()) +
                bsl::string(test_info->name());
        brp = new Carrier(contact);
        g_result = 0;
    }
    virtual void TearDown()
    {
        EXPECT_EQ(1u, reg->circuit(contact).phoneRecords().size());
    }
    ~EzhicBundleTest() { delete brp; }
    MonotonicDummy timer;
    Registry *reg;
    Contact contact;
    Carrier *brp;
};

TEST_F(EzhicBundleTest, BundleWithNullaryFunc)
{
    EXPECT_EQ(Record::SUCCESS, brp->call(&NullaryFChar));
    EXPECT_EQ(char(17), g_result);
}

TEST_F(EzhicBundleTest, BundleWithNullaryVoidFunc)
{
    EXPECT_EQ(Record::SUCCESS, brp->call(&NullaryFVoid));
}

TEST_F(EzhicBundleTest, BundleWithNullaryConstMemFunc)
{
    BNullaryTestChar obj;
    typedef char (BNullaryTestChar::*cmptr_t)() const;
    cmptr_t member = &BNullaryTestChar::constmember;
    EXPECT_EQ(Record::SUCCESS, brp->call_m(member, obj));
    EXPECT_EQ(char(25), g_result);
}

TEST_F(EzhicBundleTest, BundleWithNullaryMemFunc)
{
    BNullaryTestChar obj;
    typedef char (BNullaryTestChar::*mptr_t)();
    mptr_t member = &BNullaryTestChar::member;
    EXPECT_EQ(Record::SUCCESS, brp->call_m(member, obj));
    EXPECT_EQ(char(27), g_result);
}

TEST_F(EzhicBundleTest, BundleWithNullaryConstMemFuncVoid)
{
    BNullaryTestVoid obj;
    typedef void (BNullaryTestVoid::*cmptr_t)() const;
    cmptr_t member = &BNullaryTestVoid::constmember;
    EXPECT_EQ(Record::SUCCESS, brp->call_m(member, obj));
    EXPECT_EQ(char(30), g_result);
}

TEST_F(EzhicBundleTest, BundleWithNullaryMemFuncVoid)
{
    BNullaryTestVoid obj;
    typedef void (BNullaryTestVoid::*mptr_t)();
    mptr_t member = &BNullaryTestVoid::member;
    EXPECT_EQ(Record::SUCCESS, brp->call_m(member, obj));
    EXPECT_EQ(char(32), g_result);
}

TEST_F(EzhicBundleTest, BundleWithNullaryFunctor)
{
    EXPECT_EQ(Record::SUCCESS, brp->call(BNullaryTestChar()));
    EXPECT_EQ(char(23), g_result);
}

TEST_F(EzhicBundleTest, BundleWithNullaryVoidFunctor)
{
    EXPECT_EQ(Record::SUCCESS, brp->call(BNullaryTestVoid()));
}

TEST_F(EzhicBundleTest, BundleWithUnaryFunc)
{
    EXPECT_EQ(Record::SUCCESS, brp->call(UnaryFChar, 11));
    EXPECT_EQ(char(19), g_result);
}

TEST_F(EzhicBundleTest, BundleWithUnaryVoidFunc)
{
    EXPECT_EQ(Record::SUCCESS, brp->call(UnaryFVoid, 11));
    EXPECT_EQ(char(0), g_result);
}

TEST_F(EzhicBundleTest, BundleWithUnaryFunctor)
{
    EXPECT_EQ(Record::SUCCESS, brp->call(BUnaryTestChar(), 11));
    EXPECT_EQ(char(29), g_result);
}

TEST_F(EzhicBundleTest, BundleWithUnaryVoidFunctor)
{
    EXPECT_EQ(Record::SUCCESS, brp->call(BUnaryTestVoid(), 11));
    EXPECT_EQ(char(34), g_result);
}

TEST_F(EzhicBundleTest, BundleWithUnaryConstMemFunc)
{
    BUnaryTestChar obj;
    typedef char (BUnaryTestChar::*cmptr_t)(int) const;
    cmptr_t member = &BUnaryTestChar::constmember;
    EXPECT_EQ(Record::SUCCESS, brp->call_m(member, obj, 1));
    EXPECT_EQ(char(31), g_result);
}

TEST_F(EzhicBundleTest, BundleWithUnaryConstMemRefFunc)
{
    BUnaryTestChar obj;
    typedef char (BUnaryTestChar::*cmptr_t)(int &) const;
    cmptr_t member = &BUnaryTestChar::constrefmember;
    int lvalue = -1;
    EXPECT_EQ(Record::SUCCESS, brp->call_m(member, obj, bsl::ref(lvalue)));
    EXPECT_EQ(char(33), g_result);
    EXPECT_EQ(char(33), lvalue);
}

TEST_F(EzhicBundleTest, BundleWithUnaryMemFunc)
{
    BUnaryTestChar obj;
    typedef char (BUnaryTestChar::*cmptr_t)(int);
    cmptr_t member = &BUnaryTestChar::member;
    EXPECT_EQ(Record::SUCCESS, brp->call_m(member, obj, 1));
    EXPECT_EQ(char(35), g_result);
}

TEST_F(EzhicBundleTest, BundleWithBinaryMemFunc)
{
    BBinaryTestChar obj;
    typedef char (BBinaryTestChar::*cmptr_t)(int, float);
    cmptr_t member = &BBinaryTestChar::member;
    EXPECT_EQ(Record::SUCCESS, brp->call_m(member, obj, 1, 1.0));
    EXPECT_EQ(char(43), g_result);
}

TEST_F(EzhicBundleTest, BundleWithBinaryFunc)
{
    EXPECT_EQ(Record::SUCCESS, brp->call(BinaryFChar, 11, 1.0));
    EXPECT_EQ(char(21), g_result);
}

TEST_F(EzhicBundleTest, BundleWithBinaryFunctor)
{
    EXPECT_EQ(Record::SUCCESS, brp->call(BUnaryTestChar(), 11));
    EXPECT_EQ(char(29), g_result);
}

TEST_F(EzhicBundleTest, FailingPhoneCallCaught)
{
    EXPECT_EQ(Record::EXCEPTION, brp->call(&Throw));
    EXPECT_EQ(Record::EXCEPTION,
              reg->circuit(contact).phoneRecords().back().type());
}

TEST(EzhicBundleTestS, AlternativeCtor)
{
    Circuit c;
    Bundle b;
    MonotonicDummy timer;
    Carrier br(c, b, &timer);
    br.call(BUnaryTestChar(), 11);
    EXPECT_EQ(1u, c.phoneRecords().size());
}
