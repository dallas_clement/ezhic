#include "ezhic/Carrier.h"
#include "ezhic/Registry.h"

namespace ezhic {

Carrier::Carrier(const Contact &contact)
: d_contact(contact)
, d_circuit(ezreg::circuit(contact))
, d_bundle(ezreg::getBundle(contact))
, d_timer(ezreg::timer())
{}

// will operate on user-owned circuit, bundle, timer
Carrier::Carrier(Circuit &c, const Bundle &b, Timer *t)
: d_contact(""), d_circuit(c), d_bundle(b), d_timer(t)
{}
}  // namespace ezhic