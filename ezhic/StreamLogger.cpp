#include "ezhic/StreamLogger.h"
#include "ezhic/PhoneCall.h"

#include <ostream>

// the record at this point may differ from the final record written to
// record-trace
// if, e.g., timeout-checker will run after this call and tag the phoneCall as
// timeout
ezhic::Record
ezhic::StreamLogger::acceptUpdate(const PhoneCall *phoneCall) const
{
    if (phoneCall) {
        d_out << phoneCall->record() << '\n';
        return phoneCall->record();
    }
    return Record();
}
