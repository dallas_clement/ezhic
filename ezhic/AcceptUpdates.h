#ifndef INCLUDED_ACCEPTINTERFACE
#define INCLUDED_ACCEPTINTERFACE

#include "ezhic/Record.h"
#include <bsl_vector.h>

namespace ezhic {
class PhoneCall;  // forward

class AcceptUpdates
{
  public:
    virtual ~AcceptUpdates() {}
    virtual Record acceptUpdate(const PhoneCall *phoneCall) const;
    virtual AcceptUpdates *clone() const = 0;
};

template <typename Derived>
class AcceptUpdatesCloneable : public AcceptUpdates
{
  public:
    virtual AcceptUpdates *clone() const
    {
        return new Derived(static_cast<Derived const &>(*this));
    }
};

class TimeoutTagger : public AcceptUpdatesCloneable<TimeoutTagger>
{
  public:
    explicit TimeoutTagger(Record::duration_t threshold);
    Record acceptUpdate(const PhoneCall *phoneCall) const;
    const Record::duration_t d_threshold;
};

// will call all acceptors and return the record of the LAST
class AcceptorsArray : public AcceptUpdatesCloneable<AcceptorsArray>
{
  public:
    AcceptorsArray() {}
    ~AcceptorsArray();
    AcceptorsArray(const AcceptorsArray &);

    // Own a clone of the given object implementing AcceptUpdates
    // Can store more than one
    void cloneIn(const AcceptUpdates &acpt);
    Record acceptUpdate(const PhoneCall *phoneCall) const;

  protected:
    AcceptorsArray &operator=(const AcceptorsArray &);
    bsl::vector<AcceptUpdates *> d_acceptors;
};
}  // namespace ezhic
#endif  // EZHIC_ACCEPTINTERFACE_H
