#ifndef INCLUDED_INTENTIONTAGGER
#define INCLUDED_INTENTIONTAGGER

#include "ezhic/AcceptUpdates.h"
#include "ezhic/Record.h"

#include <vector>

namespace ezhic {

class Circuit;

// forward declarations of functors
namespace Circuitry {
class PhoneRecordPredicate;
}

// Stateless utility: will use injected phone records to determine whether
// to check for an answer, to resume calling regularly,
// or if the calls should start going straight to voicemail
class CallingStrategy
{
  public:
    CallingStrategy() {}
    CallingStrategy(const CallingStrategy &);
    CallingStrategy &operator=(const CallingStrategy &);
    void swap(CallingStrategy &rhs);

    virtual ~CallingStrategy();

    // The call intention depends on the health of the circuit.
    // GOOD: should call iff no checker wants to go straight to voicement based
    // on the phone records BAD: should call if either: a) the phone records
    // are good enough to be resume calling regularly
    //                             b) we want to check to see if the contact
    //                             will answer
    virtual Record::CallIntention operator()(Circuit &circuit) const;

    enum CheckerType
    {
        SHOULD_BREAK,  // Predicates that we should start going straight to
                       // voicemail
        SHOULD_MEND,   // Predicates that we should resume calling the contact
                       // regularly
        SHOULD_PROBE,  // Predicates for checking if the contact will answer,
                       // given we are going to voicemail
        NONE
    };

    // Own a clone of the given object implementing PhoneRecordPredicate
    // Can store more than one
    CallingStrategy &cloneIn(CheckerType type,
                             const Circuitry::PhoneRecordPredicate &checker);

  private:
    bsl::vector<const Circuitry::PhoneRecordPredicate *> d_checkers[NONE];
};
}  // namespace ezhic
#endif  // INCLUDED_INTENTIONTAGGER
