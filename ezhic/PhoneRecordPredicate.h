#ifndef INCLUDED_CIRCUITRY
#define INCLUDED_CIRCUITRY

#include "ezhic/Record.h"

namespace ezhic {

namespace Circuitry {
class PhoneRecordPredicate
{
  public:
    virtual ~PhoneRecordPredicate() {}
    // Const as circuitry (breakers, menders, probers) should be stateless.
    virtual bool operator()(const PhoneRecords &phoneRecords) const
    {
        return false;
    }
    virtual PhoneRecordPredicate *clone() const = 0;
};

template <typename Derived>
class PhoneRecordPredicateCloneable : public PhoneRecordPredicate
{
  public:
    virtual PhoneRecordPredicate *clone() const
    {
        return new Derived(static_cast<Derived const &>(*this));
    }
};

// ShouldMend: check if a broken circuit should be repaired
// ShouldProbe: check if the unreliable phoneCall may be probed
// ShouldBreak: check if the state of failures for a contact is intolerable
}  // namespace Circuitry
}  // namespace ezhic

#endif  // EZHIC_CIRCUITRY_H
