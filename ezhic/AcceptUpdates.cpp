#include "ezhic/AcceptUpdates.h"
#include "ezhic/PhoneCall.h"

namespace ezhic {

Record AcceptUpdates::acceptUpdate(const PhoneCall *phoneCall) const
{
    return phoneCall ? phoneCall->record() : Record();
}

TimeoutTagger::TimeoutTagger(Record::duration_t threshold)
: d_threshold(threshold)
{}

Record TimeoutTagger::acceptUpdate(const PhoneCall *phoneCall) const
{
    Record record = AcceptUpdates::acceptUpdate(phoneCall);
    if (record.hasKnownDuration() && record.duration() > d_threshold)
        record.type(Record::TIMEOUT);
    return record;
}

AcceptorsArray::AcceptorsArray(const AcceptorsArray &rhs)
{
    for (bsl::vector<AcceptUpdates *>::const_iterator it =
             rhs.d_acceptors.begin();
         it != rhs.d_acceptors.end();
         ++it) {
        d_acceptors.push_back((*it)->clone());
    }
}

void AcceptorsArray::cloneIn(const AcceptUpdates &acpt)
{
    d_acceptors.push_back(acpt.clone());
}

Record AcceptorsArray::acceptUpdate(const PhoneCall *phoneCall) const
{
    Record record = AcceptUpdates::acceptUpdate(phoneCall);
    for (bsl::vector<AcceptUpdates *>::const_iterator it = d_acceptors.begin();
         it != d_acceptors.end();
         ++it) {
        record = (*it)->acceptUpdate(phoneCall);
    }
    return record;
}

AcceptorsArray::~AcceptorsArray()
{
    for (bsl::vector<AcceptUpdates *>::iterator it = d_acceptors.begin();
         it != d_acceptors.end();
         ++it) {
        delete *it;
    }
}
}  // namespace ezhic