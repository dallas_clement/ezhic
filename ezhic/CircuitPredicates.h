#ifndef INCLUDED_CIRCUITPREDICATES
#define INCLUDED_CIRCUITPREDICATES

#include "ezhic/AcceptUpdates.h"
#include "ezhic/Circuit.h"
#include "ezhic/PhoneRecordPredicate.h"
#include "ezhic/Travellers.h"

namespace ezhic {
class PhoneCall;  // forward
class Registry;

namespace Circuitry {

using namespace Traversal;

typedef IteratorWrapper<PhoneRecords::const_iterator> RecordsIt;

inline RecordsIt iteratorForRecords(const PhoneRecords &phoneRecords)
{
    return RecordsIt(phoneRecords.begin(), phoneRecords.end());
}

template <typename Iterator, typename Predicate>
bool moreThanKMatch(Iterator it, Predicate predicate, size_t K)
{
    size_t acc;
    for (acc = 0; it && acc <= K; ++it) {
        acc += predicate(*it);
    }
    return acc > K;
};

const long ALL_RECORDS(-1);

// Considers S records:
// S == |phoneRecords| if M>=|phoneRecords| or M == ALL_RECORDS (else S == M)
// Returns: Whether more than K of last S records match the predicate
template <typename Predicate>
bool moreThanKInLastM(size_t k,
                      long m,
                      const PhoneRecords &phoneRecords,
                      Predicate predicate)
{
    if (m == -1)
        m = phoneRecords.size();
    AtMostNTraveller<RecordsIt> it(iteratorForRecords(phoneRecords), m);
    return moreThanKMatch(it, predicate, k);
}

class CheckMoreThanKInM
{
  public:
    CheckMoreThanKInM(size_t k, long m = ALL_RECORDS);

    static size_t percentageToSize(size_t size, long m, unsigned percents);

  protected:
    const size_t d_k;
    const long d_m;
};

struct IsRecord
{
    IsRecord(Record::Type record) : d_record(record) {}
    bool operator()(const Record &record) { return d_record == record.type(); }
    const Record::Type d_record;
};

template <typename Record::Type record>
class CheckMoreThanKEventsInM
: public CheckMoreThanKInM,
  public PhoneRecordPredicateCloneable<CheckMoreThanKEventsInM<record> >
{
  public:
    CheckMoreThanKEventsInM(size_t k, long m = ALL_RECORDS)
    : CheckMoreThanKInM(k, m)
    {}
    bool operator()(const PhoneRecords &phoneRecords) const
    {
        return moreThanKInLastM(d_k, d_m, phoneRecords, IsRecord(record));
    }
};

template <typename Record::Type record>
class CheckMoreThanKPercentEventsInM
: public CheckMoreThanKInM,
  public PhoneRecordPredicateCloneable<CheckMoreThanKPercentEventsInM<record> >
{
  public:
    CheckMoreThanKPercentEventsInM(size_t percents, long m = ALL_RECORDS)
    : CheckMoreThanKInM(percents, m)
    {}
    bool operator()(const PhoneRecords &phoneRecords)
    {
        size_t k = percentageToSize(phoneRecords.size(), d_m, d_k);
        return moreThanKInLastM(k, d_m, phoneRecords, IsRecord(record));
    }
};

typedef CheckMoreThanKEventsInM<Record::SUCCESS> RepairAfterKSuccessInM;
typedef CheckMoreThanKEventsInM<Record::TIMEOUT> BreakAfterKTimeoutsInM;
typedef CheckMoreThanKEventsInM<Record::EXCEPTION> BreakAfterKExceptionsInM;
typedef CheckMoreThanKEventsInM<Record::SUCCESS> RepairAfterKSuccessInM;
typedef CheckMoreThanKPercentEventsInM<Record::SUCCESS>
    RepairAfterKPercentsSuccessInM;
typedef CheckMoreThanKPercentEventsInM<Record::TIMEOUT>
    BreakAfterKPercentsTimeoutsInM;
typedef CheckMoreThanKPercentEventsInM<Record::EXCEPTION>
    BreakAfterKPercentsExceptionsInM;

// any failure (e.g., exception, timeout, invalid result
class BreakAfterKFailsInM : public CheckMoreThanKInM
{
  public:
    BreakAfterKFailsInM(size_t k, long m = ALL_RECORDS);

    bool operator()(const PhoneRecords &phoneRecords) const;
};

// any failure (e.g., exception, timeout, invalid result
class BreakAfterKPercentsFailsInM : public CheckMoreThanKInM
{
  public:
    BreakAfterKPercentsFailsInM(unsigned percents, long m = ALL_RECORDS);

    bool operator()(const PhoneRecords &phoneRecords) const;
};

// probe if there were more than K records after breaking or probing
class ProbeAfterK : public CheckMoreThanKInM,
                    public PhoneRecordPredicateCloneable<ProbeAfterK>
{
  public:
    ProbeAfterK(size_t k);
    bool operator()(const PhoneRecords &phoneRecords) const;
};
}  // namespace Circuitry
}  // namespace ezhic
#endif
