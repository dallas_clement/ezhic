#ifndef INCLUDED_CARRIER_H
#define INCLUDED_CARRIER_H

#include "ezhic/Bundle.h"
#include "ezhic/Circuit.h"
#include "ezhic/PhoneCall.h"
#include "ezhic/Registry.h"
#include "ezhic/Timer.h"

namespace ezhic {
struct Carrier
{
    // will use global registry to initialize
    explicit Carrier(const Contact &contact);
    // will operate on user-owned circuit, bundle, timer
    Carrier(Circuit &c, const Bundle &b, Timer *t);
    virtual ~Carrier() {}
    const Contact d_contact;
    Circuit &d_circuit;
    const Bundle &d_bundle;
    const Timer *d_timer;

    //======== 0 arguments ===================================
    template <typename Callable>
    Record::Type call(Callable callable);

    template <typename Method, typename Obj>
    Record::Type call_m(Method callable, Obj &ref);

    //======== 1 argument ===================================
    template <typename Callable, typename Argument>
    Record::Type call(Callable callable, Argument arg);

    template <typename Method, typename Obj, typename Argument>
    Record::Type call_m(Method callable, Obj &ref, Argument arg);

    //======== 2 arguments ===================================
    template <typename Callable, typename Argument1, typename Argument2>
    Record::Type call(Callable callable, Argument1 arg1, Argument2 arg2);

    template <typename Method,
              typename Obj,
              typename Argument1,
              typename Argument2>
    Record::Type
    call_m(Method callable, Obj &ref, Argument1 arg1, Argument2 arg2);
};

// Implementation

// One macro to rule them all...
#ifndef VariadicCallInstrumentator
#define VariadicCallInstrumentator(FUNC, ...)                                 \
    PhoneCall scope__phoneCall(d_contact);                                    \
    scope__phoneCall.timer(d_timer);                                          \
    Circuit &scope__circuit = d_circuit;                                      \
    const Bundle &_scope_bundle = d_bundle;                                   \
    Record::CallIntention intention =                                         \
        _scope_bundle.nextRunIntention(scope__circuit);                       \
    scope__phoneCall.record().intention(intention);                           \
    const bool shouldCall = intention != Record::STRAIGHT_TO_VOICEMAIL;       \
    if (shouldCall) {                                                         \
        try {                                                                 \
            scope__phoneCall.start();                                         \
            FUNC(__VA_ARGS__);                                                \
            scope__phoneCall.done();                                          \
        } catch (...) {                                                       \
            scope__phoneCall.record().type(Record::EXCEPTION);                \
            if (_scope_bundle.isRethrow()) {                                  \
                Record record =                                               \
                    _scope_bundle.acceptUpdate(&scope__phoneCall);            \
                scope__phoneCall.record(record);                              \
                scope__circuit.acceptUpdate(&scope__phoneCall);               \
                throw;                                                        \
            }                                                                 \
        } /* end try-catch */                                                 \
    }     /* end should call */                                               \
    else {                                                                    \
        scope__phoneCall.putTimestamp();                                      \
    }                                                                         \
    Record record = _scope_bundle.acceptUpdate(&scope__phoneCall);            \
    scope__phoneCall.record(record);                                          \
    scope__circuit.acceptUpdate(&scope__phoneCall);                           \
    return scope__phoneCall.record().type();
#endif

//======== 0 arguments ===================================
template <typename Callable>
Record::Type Carrier::call(Callable callable)
{
    VariadicCallInstrumentator(callable);
}

template <typename Method, typename Obj>
Record::Type Carrier::call_m(Method callable, Obj &ref)
{
    VariadicCallInstrumentator(((ref).*(callable)));
};

//======== 1 argument ===================================
template <typename Callable, typename Argument>
Record::Type Carrier::call(Callable callable, Argument arg)
{
    VariadicCallInstrumentator(callable, arg);
};

template <typename Method, typename Obj, typename Argument>
Record::Type Carrier::call_m(Method callable, Obj &ref, Argument arg)
{
    VariadicCallInstrumentator(((ref).*(callable)), arg);
};

//======== 2 arguments ===================================
template <typename Callable, typename Argument1, typename Argument2>
Record::Type Carrier::call(Callable callable, Argument1 arg1, Argument2 arg2)
{
    VariadicCallInstrumentator(callable, arg1, arg2);
};

template <typename Method,
          typename Obj,
          typename Argument1,
          typename Argument2>
Record::Type
Carrier::call_m(Method callable, Obj &ref, Argument1 arg1, Argument2 arg2)
{
    VariadicCallInstrumentator(((ref).*(callable)), arg1, arg2);
};

// Keep it secret! Keep it safe!
#undef VariadicCallInstrumentator

}  // namespace ezhic

#endif  // EZHIC_BRUNNER_H
