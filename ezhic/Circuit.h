#ifndef INCLUDED_CIRCUIT
#define INCLUDED_CIRCUIT

#include "ezhic/AcceptUpdates.h"
#include "ezhic/Record.h"

#include <string>

namespace ezhic {

// Stateful: holds the phone records and health status for a contact
class Circuit
{
  public:
    enum Health
    {
        BAD,
        GOOD
    };

    Circuit();
    void health(Health healthUpdate);
    const Health health() const;
    const PhoneRecords &phoneRecords() const;
    void
    clear(Health newHealth =
              GOOD);  // zap the circuit: clear phoneRecords, reset health GOOD

    virtual void acceptUpdate(const PhoneCall *phoneCall = 0);
    virtual void acceptUpdate(const PhoneCall &phoneCall);
    virtual void acceptUpdate(const Record &record);
    // the next two will lack proper timestamps:
    virtual void acceptUpdate(const Record::Type &record);
    virtual void acceptUpdate(const bsl::string &userData);

  private:
    PhoneRecords d_records;
    Health d_health;
};
}  // namespace ezhic

#endif  // INCLUDED_CIRCUIT
