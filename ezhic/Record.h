#ifndef INCLUDED_RECORD_H
#define INCLUDED_RECORD_H

#include <bsl_iosfwd.h>
#include <bsl_list.h>
#include <bsl_string.h>

namespace ezhic {
class Record
{
  public:
    typedef long duration_t;

    enum CallIntention
    {
        REGULAR_CALL,           // 0
        CHECK_FOR_ANSWER,       // 1
        STRAIGHT_TO_VOICEMAIL,  // 2
        NOT_APPLICABLE,         // 3
    };

    // TODO: should we make the separation between calls and other events
    // clearer? Also there is the separation between a call that timed out and
    // a call that returned an invalid response. This is probably not something
    // we can hide from the user
    enum Type
    {
        SUCCESS,        // 0 got a result in time
        EXCEPTION,      // 1 exception was thrown
        TIMEOUT,        // 2 call timed out
        USER,           // 3 reserved for user data
        INVALID,        // 4 returned result was invalid
        VOICEMAIL,      // 5 went straight to voicemail
        CIRCUIT_BROKE,  // 6
        CIRCUIT_FIXED,  // 7
        NIL             // 8 sentinel, not used
    };
    static const bsl::string s_typeContacts[Record::NIL + 1];

    Record(Type type = NIL, long recordedAt = -1);
    Type type() const;
    void type(Type t);
    CallIntention intention() const;
    void intention(CallIntention r);
    long recordedAtTime() const;
    void recordedAtTime(long sTime);
    long duration() const;
    void duration(long duration);
    bool hasKnownDuration() const;

    const bsl::string &userData() const;
    void userData(const bsl::string &uData);

    static const bsl::string &typeToString(Type type);

  private:
    Type d_type;
    Record::CallIntention d_intention;
    // we are deliberately fuzzy about time units
    long d_recordedAtTime;
    duration_t d_duration;
    bsl::string d_userData;
};

bool operator==(const Record &lhs, const Record &rhs);
bsl::ostream &operator<<(bsl::ostream &, const Record &);

typedef bsl::list<Record> PhoneRecords;
}  // namespace ezhic
#endif  // EZHIC_EVENT_H
