#include <algorithm>

#include "ezhic/Bundle.h"
#include "ezhic/CircuitPredicates.h"
#include "ezhic/PhoneCall.h"
#include "ezhic/Registry.h"
#include "ezhic/StreamLogger.h"

using namespace ezhic;

Bundle::Bundle() : d_acceptor(0), d_isRethrow(false) {}

Bundle::~Bundle()
{
    if (d_acceptor)
        delete d_acceptor;
}

Bundle::Bundle(const Bundle &rhs)
{
    d_callingStrategy = rhs.d_callingStrategy;
    d_isRethrow = rhs.isRethrow();
    d_acceptor = rhs.d_acceptor ? rhs.d_acceptor->clone() : 0;
}

Bundle &Bundle::operator=(const Bundle &rhs)
{
    if (this != &rhs) {
        d_callingStrategy = rhs.d_callingStrategy;
        d_isRethrow = rhs.isRethrow();
        d_acceptor = rhs.d_acceptor ? rhs.d_acceptor->clone() : 0;
    }
    return *this;
}

Bundle &Bundle::isRethrow(bool flag)
{
    d_isRethrow = flag;
    return *this;
}

bool Bundle::isRethrow() const
{
    return d_isRethrow;
}

Record Bundle::acceptUpdate(const PhoneCall *phoneCall) const
{
    return d_acceptor ? d_acceptor->acceptUpdate(phoneCall)
                      : AcceptUpdates::acceptUpdate(phoneCall);
}

Bundle &Bundle::replaceWithClone(const AcceptUpdates &acceptor)
{
    if (d_acceptor)
        delete d_acceptor;
    d_acceptor = acceptor.clone();
    return *this;
}

Record::CallIntention Bundle::nextRunIntention(const Contact &contact) const
{
    return nextRunIntention(ezreg::circuit(contact));
}

Record::CallIntention Bundle::nextRunIntention(Circuit &circuit) const
{
    return d_callingStrategy(circuit);
}

Bundle &Bundle::replaceWithClone(const CallingStrategy &nextRT)
{
    d_callingStrategy = nextRT;
    return *this;
}

Bundle ezhic::make_mbundle(bsl::ostream &loggingStream,
                           Record::duration_t timeoutThreshold,
                           bsl::pair<unsigned, unsigned> timeoutsBeforeBreak,
                           bsl::pair<unsigned, unsigned> exceptionsBeforeBreak,
                           unsigned probeAfterKEvents,
                           unsigned successBeforeMend)
{
    Bundle bundle;
    AcceptorsArray aca;
    aca.cloneIn(StreamLogger(loggingStream));
    aca.cloneIn(TimeoutTagger(timeoutThreshold));
    bundle.replaceWithClone(aca);

    CallingStrategy intentionTagger;
    intentionTagger.cloneIn(
        CallingStrategy::SHOULD_BREAK,
        Circuitry::BreakAfterKTimeoutsInM(timeoutsBeforeBreak.first,
                                          timeoutsBeforeBreak.second));

    intentionTagger.cloneIn(
        CallingStrategy::SHOULD_BREAK,
        Circuitry::BreakAfterKExceptionsInM(exceptionsBeforeBreak.first,
                                            exceptionsBeforeBreak.second));

    intentionTagger.cloneIn(CallingStrategy::SHOULD_PROBE,
                            Circuitry::ProbeAfterK(probeAfterKEvents));
    intentionTagger.cloneIn(CallingStrategy::SHOULD_MEND,
                            Circuitry::RepairAfterKSuccessInM(
                                successBeforeMend, successBeforeMend + 1));

    bundle.replaceWithClone(intentionTagger);
    return bundle;
}