#include "ezhic/PhoneCall.h"
#include "ezhic/Timer.h"
#include <bsl_exception.h>

using namespace ezhic;

PhoneCall::PhoneCall(const bsl::string &contact)
: d_contact(contact), d_timer(0)
{}

PhoneCall::PhoneCall() : d_timer(0) {}

PhoneCall::~PhoneCall() {}

void PhoneCall::timer(const Timer *timer)
{
    d_timer = timer;
}

void PhoneCall::start()
{
    if (d_timer)
        d_record.recordedAtTime(d_timer->now());
}

void PhoneCall::putTimestamp()
{
    if (d_timer)
        d_record.recordedAtTime(d_timer->now());
}

void PhoneCall::done()
{
    d_record.type(Record::SUCCESS);
    d_record.duration(
        d_timer ? long(d_timer->now() - d_record.recordedAtTime()) : -1);
}

void PhoneCall::operator()()
{  // instrument virtual call
    try {
        start();
        call();
        done();
    } catch (const bsl::exception &e) {
        d_record.type(Record::EXCEPTION);
    } catch (...) {
        d_record.type(Record::EXCEPTION);
    }
}
