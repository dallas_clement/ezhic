#include "ezhic/Circuit.h"
#include "ezhic/PhoneCall.h"

namespace ezhic {
Circuit::Circuit() : d_records(), d_health(GOOD) {}

void Circuit::health(Health healthUpdate)
{
    if (d_health == healthUpdate)
        return;
    acceptUpdate(d_health == BAD ? Record::CIRCUIT_FIXED
                                 : Record::CIRCUIT_BROKE);
    d_health = healthUpdate;
}

void Circuit::clear(Health newHealth)
{
    d_records.clear();
    health(newHealth);
}

const Circuit::Health ezhic::Circuit::health() const
{
    return d_health;
}

const PhoneRecords &ezhic::Circuit::phoneRecords() const
{
    return d_records;
}

void Circuit::acceptUpdate(const PhoneCall &phoneCall)
{
    d_records.push_front(phoneCall.record());
}

void Circuit::acceptUpdate(const ezhic::PhoneCall *phoneCall)
{
    if (phoneCall)
        acceptUpdate(*phoneCall);
}

void Circuit::acceptUpdate(const Record &record)
{
    d_records.push_front(record);
}

void Circuit::acceptUpdate(const Record::Type &record)
{
    acceptUpdate(Record(record));
}

void Circuit::acceptUpdate(const bsl::string &userData)
{
    Record e(Record::USER);
    e.userData(userData);
    d_records.push_front(e);
}
}  // namespace ezhic