#ifndef INCLUDED_REGISTRY
#define INCLUDED_REGISTRY

#include "ezhic/AcceptUpdates.h"
#include "ezhic/Bundle.h"
#include "ezhic/Circuit.h"
#include "ezhic/Record.h"
#include <bsl_map.h>

/*
 * contacts are used to name executions.
 * contacts <-> executions is many-to-many
 * execution happens in a context of a bundle
 * execution statuses are stored inside circuit objects
 *
 * a bundle executes a callable with up to two arguments
 *
 * registry has a selection of named bundles
 *
 */

namespace ezhic {
class Timer;  // forward

class Registry
{
  public:
    Timer *timer() const { return d_timer; }
    // Own a clone of the given object implementing Timer.
    Registry &cloneInTimer(const Timer &timer);

    // return a ref to bundle associated with <contact>
    // if none was given, it is first set to a copy of the default bundle
    const Bundle &bundle(const Contact &contact);

    // associates <contact> with a COPY of the given bundle <b>
    // the history for the contact is invalidated and cleared
    // the health state of its circuit is set to GOOD
    const Bundle &writeBundle(const Contact &contact, const Bundle &b);

    Circuit &circuit(const Contact &contact);

    void acceptUpdate(const PhoneCall *phoneCall = 0);

    static const Contact DEFAULT_BUNDLE;
    static Registry &instance();

  private:
    Registry();
    Registry(const Registry &);
    Registry &operator=(const Registry &);

    static Registry *s_registry;
    Timer *d_timer;
    bsl::map<Contact, Bundle> d_bundles;
    bsl::map<Contact, Circuit> d_circuits;
};

namespace ezreg {
// expose registry methods:
Timer *timer();
void cloneInTimer(const Timer &timer);
const Bundle &getBundle(const Contact &contact);
const Bundle &writeBundle(const Contact &contact, const Bundle &b);
Circuit &circuit(const Contact &contact);
}  // namespace ezreg
}  // namespace ezhic

#endif  // EZHIC_REGISTRY_H
