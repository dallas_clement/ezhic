#include "ezhic/Registry.h"
#include "ezhic/PhoneCall.h"
#include "ezhic/Timer.h"

using namespace ezhic;

// static
Registry *Registry::s_registry(0);
const Contact Registry::DEFAULT_BUNDLE("_DEFAULT_BUNDLE@Registry");

Circuit &Registry::circuit(const Contact &contact)
{
    return d_circuits[contact];
}

Registry::Registry() : d_timer(0)
{
    writeBundle(DEFAULT_BUNDLE, Bundle());
}

void Registry::acceptUpdate(const PhoneCall *phoneCall)
{
    d_circuits[phoneCall->contact()].acceptUpdate(phoneCall);
}

Registry &Registry::cloneInTimer(const Timer &timer)
{
    if (d_timer)
        delete d_timer;
    d_timer = timer.clone();
    return *this;
}

const Bundle &Registry::writeBundle(const Contact &contact, const Bundle &b)
{
    Bundle &target = d_bundles[contact];
    target = b;
    circuit(contact).clear();
    return target;
}

Registry &Registry::instance()
{
    if (s_registry)
        return *s_registry;
    s_registry = new Registry();
    return *s_registry;
}

const Bundle &Registry::bundle(const Contact &contact)
{
    bsl::map<Contact, Bundle>::iterator it = d_bundles.find(contact);
    if (it == d_bundles.end()) {
        it = d_bundles.insert(bsl::make_pair(contact, bundle(DEFAULT_BUNDLE)))
                 .first;
    }
    return it->second;
}

namespace ezhic {
namespace ezreg {
Timer *timer()
{
    return Registry::instance().timer();
}
void cloneInTimer(const Timer &timer)
{
    Registry::instance().cloneInTimer(timer);
}
const Bundle &getBundle(const Contact &contact)
{
    return Registry::instance().bundle(contact);
}
const Bundle &writeBundle(const Contact &contact, const Bundle &b)
{
    return Registry::instance().writeBundle(contact, b);
}
Circuit &circuit(const Contact &contact)
{
    return Registry::instance().circuit(contact);
}
}  // namespace ezreg
}  // namespace ezhic