#include "ezhic/CircuitPredicates.h"
#include <bsl_algorithm.h>
#include <bsl_stdexcept.h>

namespace ezhic {
namespace Circuitry {

// translate percents to actual number (mind the ALL_HISTORY convention...)
size_t CheckMoreThanKInM::percentageToSize(size_t totalSize,
                                           long sizeOfInterestOrAll,
                                           unsigned percents)
{
    size_t tsize = sizeOfInterestOrAll == ezhic::Circuitry::ALL_RECORDS
                       ? totalSize
                       : bsl::min(size_t(sizeOfInterestOrAll), totalSize);
    return tsize * percents / 100;
}

// Helper predicates ======================

struct IsSuccessOrVoicemail
{
    bool operator()(const Record &record)
    {
        return record.type() == Record::SUCCESS ||
               record.type() == Record::VOICEMAIL;
    }
};

struct IsNotEvent
{
    IsNotEvent(Record::Type record) : d_record(record) {}
    bool operator()(const Record &record) { return d_record != record.type(); }
    const Record::Type d_record;
};

bool BreakAfterKFailsInM::operator()(const PhoneRecords &phoneRecords) const
{
    return moreThanKInLastM(
        d_k, d_m, phoneRecords, IsNotEvent(Record::SUCCESS));
}

bool BreakAfterKPercentsFailsInM::
operator()(const PhoneRecords &phoneRecords) const
{
    size_t k = percentageToSize(phoneRecords.size(), d_m, d_k);
    return moreThanKInLastM(k, d_m, phoneRecords, IsNotEvent(Record::SUCCESS));
}

BreakAfterKFailsInM::BreakAfterKFailsInM(size_t k, long m)
: CheckMoreThanKInM(k, m)
{}

BreakAfterKPercentsFailsInM::BreakAfterKPercentsFailsInM(unsigned percents,
                                                         long m)
: CheckMoreThanKInM(percents, m)
{}

ProbeAfterK::ProbeAfterK(size_t k) : CheckMoreThanKInM(k, ALL_RECORDS) {}

// check if probing test loop should continue
static bool probingChecker(const Record &record)
{
    if (Record::CIRCUIT_BROKE == record.type())
        return false;
    if (record.intention() == Record::CHECK_FOR_ANSWER)
        return Record::SUCCESS == record.type();

    return true;
}

// probe if there were more than K events after breaking or after last failed
// probing
bool ProbeAfterK::operator()(const PhoneRecords &phoneRecords) const
{
    size_t count = 0;
    PhoneRecords::const_iterator it = phoneRecords.begin();
    while (it != phoneRecords.end() && probingChecker(*it) && count <= d_k) {
        ++count;
        ++it;
    }
    return count > d_k;
}

CheckMoreThanKInM::CheckMoreThanKInM(size_t k, long m) : d_k(k), d_m(m) {}
}  // namespace Circuitry
}  // namespace ezhic