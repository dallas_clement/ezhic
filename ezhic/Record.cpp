#include "ezhic/Record.h"

#include <iostream>

namespace ezhic {
// static
const bsl::string Record::s_typeContacts[Record::NIL + 1];
// {
//    bsl::string("SUCCESS"),
//    bsl::string("EXCEPTION"),    // what is going on here?
//    bsl::string("VOICEMAIL"),
//    bsl::string("TIMEOUT"),
//    bsl::string("INVALID"),
//    bsl::string("CIRCUIT_BROKE"),
//    bsl::string("CIRCUIT_PROBED"),
//    bsl::string("CIRCUIT_FIXED"),
//    bsl::string("USER"),
//    bsl::string("NIL")};

Record::Type Record::type() const
{
    return d_type;
}

void Record::type(Record::Type t)
{
    d_type = t;
}

Record::Record(Record::Type type, long recordedAt)
: d_type(type)
, d_intention(Record::REGULAR_CALL)
, d_recordedAtTime(recordedAt)
, d_duration(-1)
{}

bool operator==(const Record &lhs, const Record &rhs)
{
    return lhs.type() == rhs.type() && lhs.intention() == rhs.intention() &&
           lhs.recordedAtTime() == rhs.recordedAtTime() &&
           lhs.duration() == rhs.duration() &&
           lhs.userData() == rhs.userData();
}

bsl::ostream &operator<<(bsl::ostream &out, const Record &record)
{
    out << '{' << "\"type\" : " << record.type()
        << ", \"intention\" : " << record.intention()
        << ", \"recordedAtTime\" : " << record.recordedAtTime()
        << ", \"duration\" : " << record.duration();
    if (!record.userData().empty())
        out << ", \"user\" : \"" << record.userData() << '"';
    return out << "}";
}

Record::CallIntention Record::intention() const
{
    return d_intention;
}
void Record::intention(CallIntention r)
{
    d_intention = r;
}

long Record::duration() const
{
    return d_duration;
}

void Record::recordedAtTime(long sTime)
{
    d_recordedAtTime = sTime;
}

// depending on timer accuracy, default duration
// could be zero and > could be used instead of >=
bool Record::hasKnownDuration() const
{
    return d_duration >= 0;
}

long Record::recordedAtTime() const
{
    return d_recordedAtTime;
}

void Record::duration(long duration)
{
    d_duration = duration;
}

const bsl::string &Record::userData() const
{
    return d_userData;
}

void Record::userData(const bsl::string &uData)
{
    d_userData = uData;
}

// static
const bsl::string &Record::typeToString(Type type)
{
    return (type < Record::NIL) ? s_typeContacts[type]
                                : s_typeContacts[Record::NIL];
}
}  // namespace ezhic