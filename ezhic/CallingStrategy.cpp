#include "ezhic/CallingStrategy.h"
#include "ezhic/CircuitPredicates.h"
#include <bsl_algorithm.h>

namespace ezhic {

struct TraceChecker
{  // just binds a history and executes functors with it
    TraceChecker(const PhoneRecords &history) : d_history(history) {}
    template <typename Predicate>
    bool operator()(const Predicate *predicate) const
    {
        return predicate ? (*predicate)(d_history) : false;
    }

    const PhoneRecords &d_history;
};

template <typename CVector, typename Predicate>
static bool hasMatchesWithPredicate(const CVector &checkers,
                                    Predicate predicate)
{
    return checkers.end() !=
           bsl::find_if(checkers.begin(), checkers.end(), predicate);
};

Record::CallIntention CallingStrategy::operator()(Circuit &circuit) const
{
    const PhoneRecords &phoneRecords = circuit.phoneRecords();
    Circuit::Health currentCircuitHealth = circuit.health();

    if (hasMatchesWithPredicate(d_checkers[SHOULD_BREAK],
                                TraceChecker(phoneRecords))) {
        circuit.health(Circuit::BAD);
        return Record::STRAIGHT_TO_VOICEMAIL;
    }

    if (currentCircuitHealth == Circuit::GOOD)
        return Record::REGULAR_CALL;

    // circuit state is bad
    if (hasMatchesWithPredicate(d_checkers[SHOULD_MEND],
                                TraceChecker(phoneRecords))) {
        circuit.health(Circuit::GOOD);
        return Record::REGULAR_CALL;
    }

    bool shouldProbe = hasMatchesWithPredicate(d_checkers[SHOULD_PROBE],
                                               TraceChecker(phoneRecords));
    if (shouldProbe) {
        return Record::CHECK_FOR_ANSWER;
    }
    return Record::STRAIGHT_TO_VOICEMAIL;
}

CallingStrategy::CallingStrategy(const CallingStrategy &rhs)
{
    for (unsigned t = 0; t != NONE; ++t)
        for (bsl::vector<
                 const Circuitry::PhoneRecordPredicate *>::const_iterator it =
                 rhs.d_checkers[t].begin();
             it != rhs.d_checkers[t].end();
             ++it) {
            d_checkers[t].push_back((*it)->clone());
        }
}

CallingStrategy &CallingStrategy::operator=(const CallingStrategy &rhs)
{
    if (this != &rhs) {
        for (unsigned t = 0; t != NONE; ++t)
            for (bsl::vector<const Circuitry::PhoneRecordPredicate
                                 *>::const_iterator it =
                     rhs.d_checkers[t].begin();
                 it != rhs.d_checkers[t].end();
                 ++it) {
                d_checkers[t].push_back((*it)->clone());
            }
    }
    return *this;
}

void CallingStrategy::swap(CallingStrategy &rhs)
{
    for (unsigned t = 0; t != NONE; ++t)
        rhs.d_checkers[t].swap(d_checkers[t]);
}

CallingStrategy::~CallingStrategy()
{
    for (unsigned t = 0; t != NONE; ++t)
        for (bsl::vector<const Circuitry::PhoneRecordPredicate *>::iterator
                 it = d_checkers[t].begin();
             it != d_checkers[t].end();
             ++it)
            delete *it;
}

CallingStrategy &
CallingStrategy::cloneIn(CheckerType type,
                         const Circuitry::PhoneRecordPredicate &checker)
{
    if (type < NONE)
        d_checkers[type].push_back(checker.clone());
    return *this;
}
}  // namespace ezhic
