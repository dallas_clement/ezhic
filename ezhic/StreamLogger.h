#ifndef INCLUDED_LOGGER
#define INCLUDED_LOGGER

#include "ezhic/AcceptUpdates.h"
#include <iosfwd>

namespace ezhic {
class StreamLogger : public AcceptUpdatesCloneable<StreamLogger>
{
  public:
    explicit StreamLogger(bsl::ostream &out) : d_out(out) {}
    Record acceptUpdate(const PhoneCall *phoneCall) const;

  protected:
    bsl::ostream &d_out;
};
}  // namespace ezhic

#endif  // EZHIC_LOGGER_H
