#ifndef INCLUDED_PHONE_CALL
#define INCLUDED_PHONE_CALL

#include "ezhic/Record.h"

namespace ezhic {

typedef bsl::string Contact;
class Timer;

class PhoneCall
{
  public:
    PhoneCall();
    explicit PhoneCall(const bsl::string &contact);
    virtual ~PhoneCall();

    void operator()();  // non virtual, calling virtual protected methods

    virtual void start();
    virtual void done();

    void timer(const Timer *timer);

    const Contact &contact() const { return d_contact; }
    void contact(const Contact &contact) { d_contact = contact; }
    const Record &record() const { return d_record; }
    Record &record() { return d_record; }
    void record(const Record &record) { d_record = record; }
    void putTimestamp();

  protected:
    virtual void call() {}
    Contact d_contact;
    Record d_record;
    const Timer *d_timer;

  private:
    PhoneCall(const PhoneCall &);
    PhoneCall &operator=(const PhoneCall &);
};

}  // namespace ezhic

#endif  // EZHIC_PHONECALL_H
