cmake_minimum_required(VERSION 3.5)
project(ezhic)

# until proper cmake integration, use BDE_INSTALL_BASE environment variable to
# specify bde integration

add_library(ezhic
        ezhic/AcceptUpdates.cpp
        ezhic/Carrier.cpp
        ezhic/Bundle.cpp
        ezhic/Record.cpp
        ezhic/PhoneCall.cpp
        ezhic/Registry.cpp
        ezhic/StreamLogger.cpp
        ezhic/Timer.cpp
        ezhic/Circuit.cpp
        ezhic/PhoneRecordPredicate.cpp
        ezhic/CircuitPredicates.cpp
        ezhic/CallingStrategy.cpp
        ezhic/Travellers.cpp)

target_include_directories(ezhic PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
        $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>
        $<INSTALL_INTERFACE:include>
        $ENV{BDE_INSTALL_BASE}/include/bsl
        )
target_link_libraries(ezhic PUBLIC $ENV{BDE_INSTALL_BASE}/lib/libbsl.a)  # is public correct?

add_library(ezhic::ezhic ALIAS ezhic)

install(TARGETS ezhic EXPORT ezhicTargets
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
        RUNTIME DESTINATION bin
        INCLUDES DESTINATION include)

install(DIRECTORY ezhic DESTINATION include FILES_MATCHING PATTERN "*.h")

install(EXPORT ezhicTargets
        FILE ezhicTargets.cmake
        NAMESPACE ezhic::
        DESTINATION lib/cmake/ezhic)


# Tests  TODO: - specify optional install?

add_subdirectory(tests)
add_executable(endToEnd tests/end2end/endtoend.cpp)

target_link_libraries(endToEnd ezhic::ezhic)
target_compile_options(endToEnd PRIVATE "-Werror" "-std=c++11")


