### Motivation
Your code calls unreliable agents:
databases, remote services, and inefficient algorithms.
The calls can timeout, throw exceptions, return invalid results.   
The industry has invested much time and money in devising software patterns 
that attempt to minimise the impact of these routine errors.  
For a system to be tolerant to failures, we must recognize an ongoing problem 
and eliminate the calls entirely, thus relieving both your application and the already suffering agent.   
 
We introduce a mechanism for easily instrumenting the existing calls in your application.   
The added logic is recording the execution history for the different calls and provides tunable 
"circuit breakers" to cancel calls to misbehaving agents.  

### Usage

Your code makes calls to contacts, which are identified by a string.
Each contact is associated with a single configuration manifest named *Bundle*.
The default bundle is used for contacts that were not assigned a specific configuration.

Bundles encapsulate the configuration in `CallingStrategy` and `AcceptUpdates` objects.
**Calling Strategy** is responsible for circuit breaking logic; it is invoked before the call
to determine whether the call should be made or whether we should skip it. To use an analogy,
we go straight to voicemail.
**AcceptUpdate** is called after the execution and given the running time and completion status of the execution.  
   This simple interface is designed as a seam for any task occurs after the call completes (successfully or otherwise).
 
Usage example:  
```C++
Bundle bundle = make_mbundle(
                  std::cout,
                  timeout,
                  {kTimeouts, mTimeouts},
                  {kExceptions, mExceptions},
                  kProbe,
                  kMend);
Registry::instance().writeBundle(label, bundle);
```

Here we use a utility function to create a bundle that logs events to standard output,  
defines the duration of execution to be considered a timeout, and sets the different thresholds  
for circuit breaking and mending when timeouts or exceptions occur.  
Finally, we use `writeBundle` to associate the bundle with our label.   
All future invocations under label will use this registered bundle. 

#### Using callables with Carrier
We initialize the Carrier with a contact and instrument
the call by calling `call` and `call_m` methods for functions and methods respectively.
// TODO: provide updated example of using the carrier with return values and default returns

When Carrier is constructed with a single contact, it reaches for the global registry
in order to obtain the bundle and circuit associated with the contact.
An alternative constructor allows to bypass the global registry completely and run using local structures:  
```C++
    Circuit circuit;
    Bundle bundle;
    LinuxSysTimer timer;
    Carrier carrier(circuit, bundle, &timer);
```
  

### Components ###
* **Registry**: a central database for phone records and bundles
* **Bundle**: a configuration associated with a contact
* **Record**: encapsulates information associated with a phone call or a circuit event
* **Phone Record predicates**: use phone records to perform circuit operations: breaking, fixing, probing.
* **Update acceptors**: receive a Record object, summarizing the call for a contact

#### Registry
A registry allows setting and retrieving:  
a) A timer — a system timer or a dummy timer for adding timestamps to events and measuring running duration.
b) Bundles — a bundle per contact.
c) Circuits — a circuit per contact, contains the event history for that contact.

#### Bundle
Composed of: `CallingStrategy`, `AcceptUpdates`, and `isRethrow` flag.
IntentionTagger is used prior to execution and applies circuit predicates to decide if the call should proceed.
The decision may be *REGUlAR_CALL*, *CHECK_FOR_ANSWER*, or *STRAIGHT_TO_VOICEMAIL*.
When the *isRethrow* flag is set to true, directs the runner to isRethrow any exceptions it catches.  
AcceptUpdates is given the status of the execution, even if the decision in the previous stage was *NO_RUN* or an exception was thrown.
   
#### Phone Record Predicates
We provide various simple circuit breakers triggering after observing K events of certain type in last M phone records.

* BreakAfterKTimeoutsInM  
* BreakAfterKExceptionsInM  
* BreakAfterKPercentsTimeoutsInM  
* BreakAfterKPercentsExceptionsInM  
  
Similarly, we introduce components for issuing probing calls and repairing a broken circuit:  

* ProbeAfterK  
* RepairAfterKSuccessInM  
* RepairAfterKPercentsSuccessInM  

More sophisticated predicates may be introduced by inheriting from `PhoneRecordPredicateCloneable`.
A single CallingStrategy will typically hold several predicates.
```C++
 CallingStrategy callingStrategy;

 callingStrategy.cloneIn(
     CallingStrategy::SHOULD_BREAK,
     Circuitry::BreakAfterKTimeoutsInM(2, 5));

 callingStrategy.cloneIn(
     CallingStrategy::SHOULD_BREAK,
     Circuitry::BreakAfterKExceptionsInM(2, 7));

 callingStrategy.cloneIn(
    CallingStrategy::SHOULD_PROBE,
    Circuitry::ProbeAfterK(4));

 callingStrategy.cloneIn(
     CallingStrategy::SHOULD_MEND,
     Circuitry::RepairAfterKSuccessInM(3));
```

#### Acceptors
The acceptor is called after successful execution or catching an exception thrown inside the called code.  
The role of the acceptor is to analyze the execution metadata and return a single Event object to be stored in the event trace for the contact.
However, since multiple parties may be interested in that data, the one acceptor is often a facade to many different "subscribers".   
For instance, we may want to log the execution, check if timed out, validate or sanitize the result.   
We provide a utility class `AcceptorsArray` that will fanout the update to multiple acceptors and return the result of last invocation.
    
#### Shortcomings
* Overhead — the instrumentation of the call is not particularly costly,
but it does involve a couple of lookups for strings in maps, copying objects by value, heap allocations, etc.  
We assume that the instrumented runs are not trivial tasks and usually take some time to complete,  
hence this overhead ought to be negligible. 

* Thread safety — not safe if the global registry is used by different threads.
To avoid any race condition consider protecting non-const calls to registry with your favorite synchronization primitive.

One possible solution to both is to avoid using the global registry, instead maintaining bundles and event traces local to threads. 
